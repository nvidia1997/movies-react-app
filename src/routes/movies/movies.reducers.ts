import {ReduxAction} from "../../index.typedef";
import {
    GENRES_LOADED,
    MOVIES_LOADED,
    SET_MOVIES_SORT_BY,
    SET_MOVIES_VIEW_MODE, SET_MOVIES_YEAR,
    SET_SELECTED_GENRES_IDS
} from "./movies.actions";
import {MOVIES_SORT_BY, MOVIES_YEAR_FILTER_ALL, VIEW_MODES} from "./movies.constants";

export const initialState = {
    movies: [],
    isLoadingMovies: false,

    genres: [],
    isLoadingGenres: false,
    viewMode: VIEW_MODES.GRID,
    sort_by: MOVIES_SORT_BY.VOTE_DESC,
    selectedGenresIds: [],
    year: MOVIES_YEAR_FILTER_ALL
};

export default function moviesReducer(state = initialState, action: ReduxAction) {
    switch (action.type) {
        case MOVIES_LOADED:
            return {
                ...state,
                // @ts-ignore
                movies: action.payload.movies
            };
        case GENRES_LOADED:
            return {
                ...state,
                // @ts-ignore
                genres: action.payload.genres
            };

        case SET_MOVIES_VIEW_MODE:
            return {
                ...state,
                // @ts-ignore
                viewMode: action.payload.viewMode
            };
        case SET_MOVIES_SORT_BY:
            return {
                ...state,
                // @ts-ignore
                sort_by: action.payload.sort_by
            };
        case SET_SELECTED_GENRES_IDS:
            return {
                ...state,
                // @ts-ignore
                selectedGenresIds: action.payload.selectedGenresIds
            };
        case SET_MOVIES_YEAR:
            return {
                ...state,
                // @ts-ignore
                year: action.payload.year
            };
        default:
            return state;
    }
}
