import axios from "axios";
import {MovieGenre, Movie, MoviesQueryParams} from "./movies.typedef";

const {
    REACT_APP_MOVIES_API_KEY: apiKey
} = process.env;

export async function fetchGenresList(): Promise<MovieGenre[]> {
    // @ts-ignore
    return (await axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=${apiKey}&language=en-US`)).data.genres;
}

// @ts-ignore
export async function fetchMoviesList(queryParams: MoviesQueryParams = {}): Promise<Movie[]> {
    const searchQuery = Object
        .keys(queryParams)
        // @ts-ignore
        .filter(key => typeof queryParams[key] !== "undefined" &&
            // @ts-ignore
            queryParams[key] !== null)
        // @ts-ignore
        .map(key => `${key}=${queryParams[key]}`);

    // @ts-ignore
    return (await axios.get(`https://api.themoviedb.org/4/list/1?api_key=${apiKey}&${searchQuery}`)).data.results;
}
