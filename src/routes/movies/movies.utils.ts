import {MOVIES_YEAR_FILTER_ALL} from "./movies.constants";
import {Movie, MovieGenre, YearsDropdownValue} from "./movies.typedef";

/**
 *
 * @param {[]} movies needed to show years only for existing movies
 * @return {any[]}
 */
export function getYearsList(movies: Movie[]) {
    if (!Array.isArray(movies)) {
        throw  new Error("Invalid arg type");
    }

    const _years: any[] = [MOVIES_YEAR_FILTER_ALL];
    for (let i = new Date().getFullYear(); i >= 1980; i--) {
        if (movies.some(m => new Date(m.release_date).getFullYear() === i)) {
            _years.push(i);
        }
    }

    return _years;
}

export const moviesFilter = (moviesYear: YearsDropdownValue, selectedGenresIds: number[]) => (m: Movie) => {
    return selectedGenresIds.every(gId => m.genre_ids.includes(gId)) &&
        (moviesYear === MOVIES_YEAR_FILTER_ALL || m.release_date.includes(moviesYear.toString()));
};

export const genresFilter = (movies: Movie[]) => (g: MovieGenre) => {
    return movies.some(m => m.genre_ids.includes(g.id));
};
