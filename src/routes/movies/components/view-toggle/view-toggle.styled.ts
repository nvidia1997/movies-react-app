import styled from "styled-components";

const StyledViewModeButton = styled.button`
  &.active {
    box-shadow: 0 0 0 0.2rem rgb(72 180 97 / 50%) !important;
  }
`;

const Styled = {
    StyledViewModeButton
};

export default Styled;
