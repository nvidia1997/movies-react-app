import styled from "styled-components";

const StyledThumbnail = styled.div`
  margin-bottom: 30px;
  padding: 0px;
  -webkit-border-radius: 0px;
  -moz-border-radius: 0px;
  border-radius: 0px;

  margin-bottom: 0px;
  display: inline-block;
`

const StyledMovieBox = styled.div`
  &.list-group-item {
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 30px;
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
    padding: 0 1rem;
    border: 0;
  }

  .img-event {
    display: flex;
    justify-content: center;
  }

  &.list-group-item {
    .img-event {
      float: left;
      width: 30%;
    }

    .list-group-image {
      margin-right: 10px;
    }

    .caption {
      float: left;
      width: 70%;
      margin: 0;
    }

    &:before,
    &:after {
      display: table;
      content: " ";
    }

    &:after {
      clear: both;
    }
  }
`;

const Styled = {
    StyledMovieBox,
    StyledThumbnail
};

export default Styled;
