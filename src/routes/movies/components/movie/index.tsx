import clsx from "clsx";
import React from "react";
import {useSelector} from "react-redux";
import {VIEW_MODES} from "../../movies.constants";
import {selectMoviesViewMode} from "../../movies.selectors";
import {Movie} from "../../movies.typedef";
import Styled from "./movie.styled"

interface Props {
    movie: Movie
}

export default function MovieCard(props: Props) {
    const {movie} = props;
    const viewMode = useSelector(selectMoviesViewMode);

    return (
        <Styled.StyledMovieBox
            className={clsx({
                "col-xs-4 col-lg-4": true,
                "list-group-item": viewMode === VIEW_MODES.LIST,
                "grid-group-item": viewMode === VIEW_MODES.GRID,
            })}
        >
            <Styled.StyledThumbnail className="card">
                <div className="img-event">
                    <img
                        className="group list-group-image img-fluid"
                        src={`https://image.tmdb.org/t/p/w220_and_h330_face/${movie.poster_path}`}
                        alt="movie poster"
                    />
                </div>
                <div className="caption card-body">
                    <h4 className="group card-title inner list-group-item-heading">
                        {movie.title} &nbsp;

                        <span className="badge badge-secondary">
                         {new Date(movie.release_date).getFullYear()}
                       </span>
                    </h4>
                    <p className="group inner list-group-item-text">{movie.overview}</p>
                </div>
            </Styled.StyledThumbnail>
        </Styled.StyledMovieBox>
    );
}
