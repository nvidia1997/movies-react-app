import React, {useEffect, useMemo} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Dropdown from "../../components/dropdown";
import GenresSelector from "./components/genres-selector";
import MovieCard from "./components/movie";
import ViewToggle from "./components/view-toggle";
import * as moviesActions from "./movies.actions";
import {MOVIES_SORT_BY} from "./movies.constants";
import * as moviesSelectors from "./movies.selectors";
import {getYearsList} from "./movies.utils";

function Movies() {
    const dispatch = useDispatch();
    const allMovies = useSelector(moviesSelectors.selectMovies);
    const filteredMovies = useSelector(moviesSelectors.selectFilteredMovies);
    const moviesSortBy = useSelector(moviesSelectors.selectMoviesSortBy);
    const moviesYear = useSelector(moviesSelectors.selectMoviesYear);
    const yearsList = useMemo(() => getYearsList(allMovies), [allMovies]);
    const moviesSortByList = useMemo(() => Object.values(MOVIES_SORT_BY), []);

    useEffect(() => {
        dispatch(moviesActions.loadMoviesAction({
            sort_by: moviesSortBy
        }));
    }, [dispatch, moviesSortBy]);

    useEffect(() => {
        dispatch(moviesActions.loadGenresAction());
    }, [dispatch]);

    const _onYearChange = (e: React.ChangeEvent) => {
        // @ts-ignore
        dispatch(moviesActions.setMoviesYearAction(e.target.value));
    };

    const _onMoviesSortByChange = (e: React.ChangeEvent) => {
        // @ts-ignore
        dispatch(moviesActions.sortMoviesByAction(e.target.value));
    };

    return (
        <div className="App">
            <div className="container p-5">
                <Dropdown
                    label="Year"
                    items={yearsList}
                    value={moviesYear}
                    // @ts-ignore
                    onChange={_onYearChange}
                />

                <Dropdown
                    label="Sort By"
                    items={moviesSortByList}
                    value={moviesSortBy}
                    // @ts-ignore
                    onChange={_onMoviesSortByChange}
                />

                <GenresSelector/>
                <ViewToggle/>

                <div id="movies" className="row view-group">
                    {
                        filteredMovies
                            .map(movie => <MovieCard
                                key={`movie-${movie.id}`}
                                movie={movie}
                            />)
                    }
                </div>
            </div>
        </div>
    );
}

export default Movies;
