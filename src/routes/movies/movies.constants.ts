import {ViewMode} from "./components/view-toggle/view-toggle.typedef";

export const MOVIES_SORT_BY = {
    POPULARITY_ASC: "original_order.asc",
    POPULARITY_DESC: "original_order.desc",
    RELEASE_DATE_ASC: "release_date.asc",
    RELEASE_DATE_DESC: "release_date.desc",
    TITLE_ASC: "title.asc",
    TITLE_DESC: "title.desc",
    VOTE_ASC: "vote_average.asc",
    VOTE_DESC: "vote_average.desc",
};

export const VIEW_MODES: { [key: string]: ViewMode } = {
    GRID: "GRID",
    LIST: "LIST",
};

export const MOVIES_YEAR_FILTER_ALL = "all";
