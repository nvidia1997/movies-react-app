import {MOVIES_STATE_KEY} from "./movies.actions";
import {Movie, MovieGenre, YearsDropdownValue} from "./movies.typedef";
import {genresFilter, moviesFilter} from "./movies.utils";

export const selectMovies = (state: any): Movie[] => state[MOVIES_STATE_KEY].movies || [];
export const selectFilteredMovies = (state: any): Movie[] => {
    const _selectedGenresIds = selectSelectedGenresIds(state);
    const _moviesYear: YearsDropdownValue = selectMoviesYear(state);

    return selectMovies(state)
        .filter(moviesFilter(_moviesYear, _selectedGenresIds));
};
export const selectGenres = (state: any): MovieGenre[] => state[MOVIES_STATE_KEY].genres || [];
export const selectFilteredGenres = (state: any): MovieGenre[] => {
    const _movies = selectMovies(state);
    return selectGenres(state)
        .filter(genresFilter(_movies));
};
export const selectIsLoadingMovies = (state: any): boolean => state[MOVIES_STATE_KEY].isLoadingMovies;
export const selectIsLoadingGenres = (state: any): boolean => state[MOVIES_STATE_KEY].isLoadingGenres;
export const selectMoviesViewMode = (state: any): string => state[MOVIES_STATE_KEY].viewMode;
export const selectMoviesSortBy = (state: any): string => state[MOVIES_STATE_KEY].sort_by;
export const selectSelectedGenresIds = (state: any): number[] => state[MOVIES_STATE_KEY].selectedGenresIds;
export const selectMoviesYear = (state: any): YearsDropdownValue => state[MOVIES_STATE_KEY].year;
