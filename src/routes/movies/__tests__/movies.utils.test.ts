import {MOVIES_YEAR_FILTER_ALL} from "../movies.constants";
import {Movie, MovieGenre} from "../movies.typedef";
import {genresFilter, getYearsList, moviesFilter} from "../movies.utils";

it("should throw on invalid args type [getYearsList]", () => {
    // @ts-ignore
    expect(() => getYearsList(undefined))
        .toThrow();
});

it("years[] should contain 'all' getYearsList[]", () => {
    expect(getYearsList([]))
        .toEqual(expect.arrayContaining([MOVIES_YEAR_FILTER_ALL]));
});

it("years[] should contain all these items [getYearsList]", () => {
    const movies: Movie[] = [
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString()
        },
        // @ts-ignore
        {
            release_date: new Date("2014-06-24").toString()
        }
    ];

    expect(getYearsList(movies))
        .toEqual(expect.arrayContaining([MOVIES_YEAR_FILTER_ALL, 2019, 2014]));
});

it("should return only movies with genre independent of year [moviesFilter]", () => {
    const movies: Movie[] = [
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [1, 2, 3]
        },
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [3]
        }
    ];

    const filteredMovies: Movie[] = movies
        .filter(moviesFilter(MOVIES_YEAR_FILTER_ALL, [2]));

    expect(
        filteredMovies[0] === movies[0]
        && filteredMovies.length === 1
    )
        .toBe(true);
});

it("should return whole list of movies #1 [moviesFilter]", () => {
    const movies: Movie[] = [
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [1, 2, 3]
        },
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [2, 3]
        }
    ];

    const filteredMovies: Movie[] = movies
        .filter(moviesFilter(MOVIES_YEAR_FILTER_ALL, [2, 3]));

    expect(
        filteredMovies[0] === movies[0]
        && filteredMovies[1] === movies[1]
    )
        .toBe(true);
});

it("should return whole list of movies #2 [moviesFilter]", () => {
    const movies: Movie[] = [
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [1, 2, 3]
        },
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [2, 3]
        }
    ];

    const filteredMovies: Movie[] = movies
        .filter(moviesFilter(MOVIES_YEAR_FILTER_ALL, []));

    expect(
        filteredMovies[0] === movies[0]
        && filteredMovies[1] === movies[1]
    )
        .toBe(true);
});

it("should return movies only with this year [moviesFilter]", () => {
    const movies: Movie[] = [
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [1, 2, 3]
        },
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [3]
        }
    ];

    const filteredMovies: Movie[] = movies
        .filter(moviesFilter(2014, [2, 3]));

    expect(
        filteredMovies.length === 0
    )
        .toBe(true);
});

it("should return genres for which we have movies #1 [genresFilter]", () => {
    const movies: Movie[] = [
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [1, 2, 3]
        },
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [3]
        }
    ];

    const genresIds: MovieGenre[] = [
        {
            id: 1,
            name: "Action"
        },
        {
            id: 2,
            name: "Comedy"
        },
        {
            id: 3,
            name: "Sci-Fi"
        },
        {
            id: 4,
            name: "Horror"
        }
    ];

    const filteredGenres: MovieGenre[] = genresIds.filter(genresFilter(movies));

    expect(
        filteredGenres.length === 3
    )
        .toBe(true);
});


it("should return genres for which we have movies #2 [genresFilter]", () => {
    const movies: Movie[] = [
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [3]
        },
        // @ts-ignore
        {
            release_date: new Date("2019-06-24").toString(),
            genre_ids: [3]
        }
    ];

    const genresIds: MovieGenre[] = [
        {
            id: 1,
            name: "Action"
        },
        {
            id: 2,
            name: "Comedy"
        },
        {
            id: 3,
            name: "Sci-Fi"
        },
        {
            id: 4,
            name: "Horror"
        }
    ];

    const filteredGenres: MovieGenre[] = genresIds.filter(genresFilter(movies));

    expect(
        filteredGenres.length === 1
    )
        .toBe(true);
});
